import { Component,OnInit, } from '@angular/core';
import { FormGroup, FormControl, Validators, FormsModule } from '@angular/forms';
import { CommonService } from './common.service';
import { Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
 
  public id: Number;
  public name: String;
  public address: String;


  constructor( private newService: CommonService ) { }
  Repdata: any;
  valbutton = "Save";

  ngOnInit() {
    this.newService.GetUser().subscribe(data => this.Repdata = data)
  }

  onSave = function (this: AppComponent, users: any, isValid: boolean) {
    users.mode = this.valbutton;
    this.newService.saveUser(users)
      .subscribe((data: { data: any; }) => {
        alert(data.data);
        this.ngOnInit();
      }

      )
  }

  edit = function (this: AppComponent, kk: any) {
    this.id = kk._id;
    this.name = kk.name;
    this.address = kk.address;
    this.valbutton = "Update";
  }
  delete = function (this: AppComponent, id: number) {
    this.newService.deleteUser(id)
      .subscribe(data => {
        alert(data.data); this.ngOnInit();
      }

      )
  }

}

